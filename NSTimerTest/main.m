//
//  main.m
//  NSTimerTest
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
