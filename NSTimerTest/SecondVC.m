//
//  SecondVC.m
//  NSTimerTest
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "SecondVC.h"

@interface SecondVC ()

@end

@implementation SecondVC

#pragma mark INITIALISERS

-(id) init
{
    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self = [st instantiateViewControllerWithIdentifier:@"SecondVC"];
    if (self)
    {
        
    }
    
    return self;
}

#pragma LIFECYCLE

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ACTIONS

- (IBAction)btnBackDidTapped:(id)sender
{    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
