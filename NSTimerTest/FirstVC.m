//
//  FirstVC.m
//  NSTimerTest
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "FirstVC.h"
#import "TKWeakTimerTarget.h"

#define k_timer_time 1.0f

@interface FirstVC ()<GCDTimer>
@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property TKWeakTimerTarget *timer;
@end

@implementation FirstVC

#pragma LIFECYCLE

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];

    [self startTimer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TKTIMER METHODS

- (void) startTimer
{
    _timer = [[TKWeakTimerTarget alloc]initWithInterval:k_timer_time];
    _timer.delegate = self;
    [_timer startTimer];
}

#pragma mark TKTIMER DELEGATE

- (void)timerFired:(id)sender
{
    TKWeakTimerTarget *timer = (TKWeakTimerTarget *)sender;
    
    NSLog(@"Tick %li",[timer getInstances]);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateLabelWithValue:[timer getInstances]];
    });
    
    if ([timer getInstances] == 3)
    {
        [timer cancelTimer];
        timer = nil;
        _timer = nil;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentSecondVC];
        });
    }
}

#pragma mark PRIVATE METHODS

- (void) updateLabelWithValue:(NSInteger)value
{
    self.lbl.text = [NSString stringWithFormat:@"%li",4 - value];
}

- (void) presentSecondVC
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondVC"];
    [self presentViewController:vc animated:NO completion:^{
        self.lbl.text = @"3";
    }];
}

@end
